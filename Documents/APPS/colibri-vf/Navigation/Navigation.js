// Navigation/Navigation.js

import React from 'react'
import { StyleSheet, Image } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import {createStackNavigator } from 'react-navigation-stack'
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome5';

import LoginScreen from '../src/screens/LoginScreen';
import WelcomeScreen from '../src/screens/WelcomeScreen';
import HomeScreen from '../src/screens/HomeScreen';
import AuthLoadingScreen from '../src/screens/AuthLoadingScreen';
import LogoutScreen from '../src/screens/LogoutScreen';
import SettingsScreen from '../src/screens/SettingsScreen';
import ProfilScreen from '../src/screens/ProfilScreen';


const HomeStack = createStackNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      headerTitle: "Accueil",
      headerStyle: {
        backgroundColor: '#1976D2',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }
  }
});

const ProfilStack = createStackNavigator({
  Profil: {
    screen : ProfilScreen,
    navigationOptions: {
      headerTitle: "Profil",
      headerStyle: {
        backgroundColor: '#1976D2',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }
  }
});

const SettingsStack = createStackNavigator({
  Settings: {
    screen: SettingsScreen,
    navigationOptions: {
      headerTitle: "F.A.Q",
      headerStyle: {
        backgroundColor: '#1976D2',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }
  },
});

const LogoutStack = createStackNavigator({
  Logout:{
    screen: LogoutScreen,
    navigationOptions: {
      headerTitle: "Logout",
      headerStyle: {
        backgroundColor: '#1976D2',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }
  }
});

const AppStack = createMaterialBottomTabNavigator({
  Home: {
    screen: HomeStack,
    navigationOptions: {
      title: 'Accueil',
      tabBarIcon: ({ tintColor }) => (
        <Icon name="home" size={25} color={tintColor} />
      )
    }
  },
  Profil: {
    screen: ProfilStack,
    navigationOptions: {
      title: 'Profil',
      tabBarIcon: ({ tintColor }) => (
        <Icon name="user-alt" size={25} color={tintColor} />
      )
    }
  },
  Settings: {
    screen: SettingsStack,
    navigationOptions: {
      title: 'Réglages',
      tabBarIcon: ({ tintColor }) => (
        <Icon name="cog" size={25} color={tintColor} />
      )
    }
  },
  
  Logout: {
    screen: LogoutStack,
    navigationOptions: {
      title: 'Deconnexion',
      tabBarIcon: ({ tintColor }) => (
        <Icon name="sign-out-alt" size={25} color={tintColor} />
      )
    }
  },
  

},
{
  initialRouteName: 'Home',
  activeColor: '#1976D2',
  inactiveColor: '#607D8B',
  barStyle: { backgroundColor: '#ffffff' },
}
)



const AuthStack = createStackNavigator({ 
  Welcome: {
  screen: WelcomeScreen,
  navigationOptions: {
    header:null
  }
},
  Login: {
  screen: LoginScreen,
  navigationOptions: {
  header: null
}
}
});

const styles = StyleSheet.create({
  icon: {
    width: 30,
    height: 30
  }
})


//const App = createAppContainer(LoginStackNavigator)
export default createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: AuthLoadingScreen,
      App: AppStack,
      Auth: AuthStack,
    },
    {
      initialRouteName: 'AuthLoading',
    }
  )
);
