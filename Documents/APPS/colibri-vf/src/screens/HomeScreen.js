import React from "react";
import { TouchableOpacity, FlatList, Text, Image, StyleSheet, TextInput, View, KeyboardAvoidingView, Button, ActivityIndicator } from "react-native";
//import Button from "../Components/Button";
import colors from "../config/colors";
import { List, Banner } from "react-native-paper";

const extractKey = ({ id }) => id.toString();

const rows = [
  { id: 0, text: 'View' },
  { id: 1, text: 'Text' },
  { id: 2, text: 'Image' },
  { id: 3, text: 'ScrollView' },
  { id: 4, text: 'ListView' },
]

class HomeScreen extends React.Component{

  constructor(props) {
    super(props)
    this.state = {
      films: [],
      visible: true,
    }
  }

  renderItem = ({ item }) => {
    return (
      <List.Item
    title={item.text}
    description="Item description"
    left={props => <List.Icon {...props} icon="folder" />}
  />
    )
  }

  _handleSuiviPress = () => {
    console.log("Suivi scolaire bouton button pressed")
    this.props.navigation.navigate('ChildrenList')
  }

  _displayDetailForChild = (ChildId) => {
    console.log("Display Child " + ChildId)
    // On a récupéré les informations de la navigation, on peut afficher le détail du film
    this.props.navigation.navigate('ChildDetail', {Child: ChildId})
  }

  componentDidMount(){
    fetch("https://jsonplaceholder.typicode.com/users")
    .then(response => response.json())
    .then((responseJson)=> {
      this.setState({
       loading: false,
       dataSource: responseJson
      })
    })
    .catch(error=>console.log(error)) //to catch the errors if any
    }

    renderItem=(data)=>
    <TouchableOpacity onPress={() => this.props.navigation.navigate('ChildDetail')}>
  <List.Item title={data.item.name} description={data.item.email} style={styles.list} left={props => <List.Icon {...props} icon="account" />} />
  </TouchableOpacity>
  
  render() {
    return (
      <View style={styles.container}>
        <Banner
        visible={this.state.visible}
        actions={[
          {
            label: 'Masquer',
            onPress: () => this.setState({ visible: false }),
          },

        ]}
        image={({ size }) =>
          <Image
            source={{ uri: 'https://avatars3.githubusercontent.com/u/17571969?s=400&v=4' }}
            style={{
              width: size,
              height: size,
            }}
          />
        }
      >
        Bienvenue dans ParenStud. N'hésitez pas à consulter l'onglet Aide pour plus d'informations.
      </Banner>
        <FlatList
          data= {this.state.dataSource}
          renderItem={this.renderItem}
          renderItem= {item=> this.renderItem(item)}
          keyExtractor= {item=>item.id.toString()}
          />
        <View style={styles.content}>
        
          <Button title='Suivi Scolaire (Mes enfants)' onPress={this._handleSuiviPress} />
          
          
          {/* <FlatList
            style={styles.list}
            data={children}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({item}) => (
              <ChildItem
                child={item}
                displayDetailForChild={this._displayDetailForChild}
              />
            )}
            onEndReachedThreshold={0.5}
            onEndReached={() => {
              if (this.props.page < this.props.totalPages) {
                // On appelle la méthode loadFilm du component Search pour charger plus de films
                this.props.loadChildren()
              }
            }}
          /> */}
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.WHITE,
    
  },
  heading:{
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  content:{
    flex: 8,
    alignItems: "center",

  },
  logoContainer: {
    width: "50%",
    height: "30%",
    justifyContent: "center",
    paddingTop: 50,
  },
  logo: {
    flex: 1,
    resizeMode: "contain",
    paddingTop: 20,
    width: 50,
    height: 50
  },
  textContent: {
    padding: 50,
    textAlign: "center",
  }
})

export default HomeScreen
