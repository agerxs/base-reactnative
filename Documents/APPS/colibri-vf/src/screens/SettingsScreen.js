import React from "react";
import { Text, Image, StyleSheet, TextInput, View, KeyboardAvoidingView, ActivityIndicator } from "react-native";
//import Button from "../Components/Button";
import {  Button, Subheading, List, Checkbox, Paragraph } from 'react-native-paper';
import colors from "../config/colors";
import { ImageBackground } from 'react-native'

class SettingsScreen extends React.Component{

  state = {
    expanded: true
  }

  _handlePress = () =>
    this.setState({
      expanded: !this.state.expanded
    });

    render() {
        return (
          <View style={styles.container}>
          <List.Section title="Foire aux Questions">
        <List.Accordion
          title="Qu'est ce que ParenStud ?"
          left={props => <List.Icon {...props} icon="folder" />}
        ><Paragraph>
        <Text >Créée en 2019, <Text style={styles.titleText} >Parenstud</Text> est la solution digitale qui permet aux parents de mieux suivre l'évolution scolaire de leurs enfants.
Notre application depuis sa création, permet à des centaines de parents d'accéder à distance et en quelques clics aux notes de leurs enfants. Les notes sont actualisées régulièrement pour être les plus récentes possibles. Les parents ont la possibilité de lire les commentaires des professeurs mais peuvent aussi prendre contact avec eux. 
Cette initiative vise essentiellement à  améliorer le rendement des élèves et nos partenaires privilégiés que sont les écoles , nous accompagnent dans ce sens. C'est un projet d’ordre pédagogique et social dans  car il contribue “à rehausser le niveau scolaire des élèves ivoiriens.</Text>
      </Paragraph>
        </List.Accordion>

        <List.Accordion
          title="Comment adhérer?"
          left={props => <List.Icon {...props} icon="folder" />}
          expanded={this.state.expanded}
          onPress={this._handlePress}
        >
          <List.Item title="First item" />
          <List.Item title="Second item" />
        </List.Accordion>
      </List.Section>
        </View>
          
        )
      }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.WHITE,
  },
  heading:{
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  content:{
    flex: 8,
    alignItems: "center",

  },
  logoContainer: {
    width: "50%",
    height: "30%",
    justifyContent: "center",
    paddingTop: 50,
  },
  logo: {
    flex: 1,
    resizeMode: "contain",
    paddingTop: 20,
    width: 50,
    height: 50
  },
  textContent: {
    padding: 50,
    textAlign: "center",
  }
})

export default SettingsScreen;