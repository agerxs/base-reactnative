import React from "react";
import { Text, Image, StyleSheet, TextInput, View, KeyboardAvoidingView, ActivityIndicator } from "react-native";
//import Button from "../Components/Button";
import {  Button, Subheading } from 'react-native-paper';
import colors from "../config/colors";
import { ImageBackground } from 'react-native'

class WelcomeScreen extends React.Component{

  _handleSuiviPress = () => {
    console.log("Suivi scolaire bouton button pressed")
    this.props.navigation.navigate('Login')
  }

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground 
    source={require('../Images/bg_homescreen.jpg')}
    style={{flex:1,width: '100%', height: '100%',alignItems: 'center',
    justifyContent: 'center'}} 
>
      <View style={styles.content}>
      <View style={{flex:1}}></View>
      <View style={{flex:1}}>
        <Subheading style={styles.textContent}>ParenStud est une application qui vous permet d'assurer
          le suivi scolaire de votre enfant.
        </Subheading>
        <View style={styles.buttonstyle}>
        <Button mode="contained" onPress={this._handleSuiviPress} dark >Commencer</Button>
        </View>
        </View>
      </View>
      </ImageBackground>
      </View>
      
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.WHITE,
    alignItems: "center",
    justifyContent: "center"
  },
  heading:{
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  content:{
    flex: 8,
    alignItems: "center",

  },
  logoContainer: {
    width: "50%",
    height: "30%",
    justifyContent: "center",
    paddingTop: 50,
  },
  logo: {
    flex: 1,
    resizeMode: "contain",
    paddingTop: 20,
    width: 50,
    height: 50
  },
  textContent: {
    padding: 50,
    textAlign: "center",
    color:'white'
  },
  buttonstyle:{
    alignItems:"center",
    justifyContent: "center",
    flex:1,
    flexDirection:'row',
    marginBottom: 30
  }
})

export default WelcomeScreen
