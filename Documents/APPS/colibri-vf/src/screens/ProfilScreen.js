import React from "react";
import { Text, Image, StyleSheet, TextInput, View, KeyboardAvoidingView, ActivityIndicator } from "react-native";
//import Button from "../Components/Button";
import {  Button, Subheading } from 'react-native-paper';
import colors from "../config/colors";
import { ImageBackground } from 'react-native'

class ProfilScreen extends React.Component{
    render() {
        return (
          
          <View style={styles.container}>
          <View style={styles.content}>
            <Text style={styles.textContent}>ParenStud est une application qui vous permet d'assurer
              le suivi scolaire de votre enfant.
            </Text>
          </View>
        </View>
          
        )
      }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.WHITE,
    alignItems: "center",
    justifyContent: "center"
  },
  heading:{
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  content:{
    flex: 8,
    alignItems: "center",

  },
  logoContainer: {
    width: "50%",
    height: "30%",
    justifyContent: "center",
    paddingTop: 50,
  },
  logo: {
    flex: 1,
    resizeMode: "contain",
    paddingTop: 20,
    width: 50,
    height: 50
  },
  textContent: {
    padding: 50,
    textAlign: "center",
  }
})

export default ProfilScreen;