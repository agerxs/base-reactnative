import React from "react";
import { Image, StyleSheet, View, KeyboardAvoidingView, Text, ActivityIndicator, TouchableOpacity } from "react-native";
//import Button from "../Components/Button";
import { TextInput, Button} from "react-native-paper";
import imageLogo from "../Images/logo_trans.png";
import colors from "../config/colors";
import { authentication } from '../API/AuthApi';
import AsyncStorage from 'react-native';

class LoginScreen extends React.Component{

  constructor(props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      isLoggingIn: false,
      message: ''
      
    };
    this.passwordInputRef= React.createRef()
  }

  _handleEmailChange = (username: string) => {
    this.setState({ username: username })
  };

  _handlePasswordChange = (password: string) => {
    this.setState({ password: password })
  };

  _handleEmailSubmitPress() {
    if(this.passwordInputRef.current) {
      this.passwordInputRef.current.focus()
    }
  }

  _handleLoginPress = () => {
    //alert(JSON.stringify(this.state));
    //console.log("Login button pressed")
    //console.log(this.state.username +" et " +this.state.password )

    //this.props.navigation.navigate('HomeTab')
    this.setState({isLoggingIn: true, message:''})

    const url = 'https://parenstud.herokuapp.com/login_check'
    const { username, password} = this.state;

    fetch(url,
      {
          method: "POST",
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
          },
          body: JSON.stringify({
              username: username,
              password: password
          })
      }
      )
      .then((response) => response.json())
      .then((response) => {
  
                //console.log('Warning','Username / Password Salah!')
                  if (response.token != undefined) {
                      console.log(response.token)
                      this.setState({isLoggingIn: false})
                      _storeData = async()=>{
                        try{
                          await AsyncStorage.setItem('tokenUser', response.token);
                          
                        } catch (error){
                          console.log(error);
                        }
                      }
                      this.props.navigation.navigate('App')
                        // AsyncStorage.setItem('statusUser', response.status);
                  }
                  else{
                    console.log('Warning','Username / Password Salah!')
                      this.setState({isLoggingIn: false})
                      setTimeout(() => {
                          //alert('Nom Utilisateur ou mot de passe incorrect');
                          this.setState({message: 'Nom Utilisateur ou mot de passe incorrect'});
                        }, 100); 
                  }
          }    
      )
      //.catch((error)=>console.error(error))
      .done();
    //authentication(this.state.username, this.state.password).then(data => console.log(data));
  }

  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <Image style={styles.bgImage} source={require('../Images/bg_homescreen.jpg')}/>
        <View style={styles.logoContainer}>
        <Image source={imageLogo} style={styles.logo} />
        </View>
        <View style={styles.form}>
        <View style={styles.inputContainer}>
          <TextInput style={styles.textinput}
          mode='flat'
            label='Username'
            autoCompleteType='username'
            placeholder='Username'
            value={this.state.username}
            onChangeText={username => this.setState({ username })}
            onSubmitEditing={() => this._handleEmailSubmitPress()}
          /></View>
          <View style={styles.inputContainer}>
          <TextInput style={styles.textinput}
          mode='flat'
          autoCompleteType='password'
          secureTextEntry={true}
          label='Mot de passe'
          placeholder='Mot de passe'
          value={this.state.password}
            ref={this.passwordInputRef}
            onChangeText={password => this.setState({ password })}
          />
          </View>
          {!!this.state.message && (
            <Text
                    style={{fontSize: 14, color: 'orange', padding: 5}}>
                    {this.state.message}
                </Text>
            )}
          {this.state.isLoggingIn && <ActivityIndicator />}
          <Button mode="contained" 
          disabled={this.state.isLoggingIn||!this.state.username||!this.state.password}
          style={[styles.buttonContainer]} 
          onPress={() => this._handleLoginPress()} dark compact>
            Connexion
          </Button>
        </View>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.APP_BLUE,
    alignItems: "center",
    justifyContent: "space-between"
  },
  logo: {
    flex: 1,
    width: "100%",
    resizeMode: "contain",
    alignSelf: "center",
    paddingTop: 20,
  },
  logoContainer: {
    flex: 1,
    width: "100%",
    resizeMode: "contain",
    alignSelf: "center",
    paddingTop: 50,
  },
  form: {
    flex: 1,
    justifyContent: "flex-start",
    width: "80%",
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputContainer: {
    backgroundColor: "white",
    width:'80%',
    height:45,
    marginBottom:20,
    flexDirection: 'row',
    alignItems:'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textinput:{
    flex:1,
  },
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:'80%',

  },
  loginButton: {
    backgroundColor: "#00b5ec",

    shadowColor: "#808080",
    shadowOffset: {
      width: 0,
      height: 9,
    },
    shadowOpacity: 0.50,
    shadowRadius: 12.35,

    elevation: 19,
  },
  loginText: {
    color: 'white',
  },
  bgImage:{
    flex: 1,
    position: 'absolute',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
  },
  btnText:{
    color:"white",
    fontWeight:'bold'
  }
})

export default LoginScreen
